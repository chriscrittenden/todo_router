import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter, Route} from 'react-router-dom';
import {App, SignIn} from './App';

ReactDOM.render((
    <BrowserRouter>
        <div>
            <Route exact path="/" component={SignIn}/>
            <Route path="/tasks" component={App}/>
        </div>
    </BrowserRouter>
    ), document.getElementById('root')
);

registerServiceWorker();